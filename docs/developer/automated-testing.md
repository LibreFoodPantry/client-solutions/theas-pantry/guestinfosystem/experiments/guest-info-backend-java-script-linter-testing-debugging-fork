# Automated Testing

The backend is tested locally by running:

```bash
commands/build.sh
```

The tests reside in `/testing/test-runner/test`.

These test are also run in the pipeline.
