# Guest Info System Backend Server

The GuestInfoBackend provides a REST API server that implements an OpenAPI
specification and is called by the GuestInfoFrontend. The developers
of GuestInfoFrontend are the clients of GuestInfoBackend.

## Client Guide

The API implemented by this server is in [specification/openapi.yaml](specification/openapi.yaml).
The API can be viewed:

* Using the
  [Swagger Viewer extension for VS Code](https://marketplace.visualstudio.com/items?itemName=Arjun.swagger-viewer)
  which is installed in the Dev Container for this project.
* Directly in the
  [GitLab repository for this project](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfobackend).

## Developer Guide

Getting Started

### Working Locally

1. Read [LibreFoodPantry.org](https://librefoodpantry.org/)
    and join its Discord server.
2. [Install development environment](docs/developer/install-development-environment.md)
3. Clone this repository using the following command

    ```bash
    git clone <repository-clone-url>
    ```

4. Open it in VS Code and reopen it in a devcontainer.
5. [specification/openapi.yaml](specification/openapi.yaml) contains the
   API specification that the server implements.
6. Familiarize yourself with the systems used by this project
   (see Development Infrastructure below).
7. See [the developer cheat-sheet](docs/developer/cheat-sheet.md) for common
   commands you'll likely use.

### Working with [Gitpod](https://www.gitpod.io/)

You can do your development work in the cloud with Gitpod. To use this project
in Gitpod, open [https://gitpod.io/#https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfobackend](https://gitpod.io/#https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfobackend)

It may take a little while for all the recommended VSCode extensions to be
installed.

**When you finish your work, be sure to stop your Gitpod workspace. Click
the orange Gitpod button in the lower-left, and choose
`Gitpod: Stop workspace`.**

### Development Infrastructure

* [Automated Testing](docs/developer/automated-testing.md)
* [Build System](docs/developer/build-system.md)
* [Continuous Integration](docs/developer/continuous-integration.md)
* [Dependency Management](docs/developer/dependency-management.md)
* [Development Environment](docs/developer/development-environment.md)
* [Documentation System](docs/developer/documentation-system.md)
* [Version Control System](docs/developer/version-control-system.md)
