const {
    replaceGuest,
    createGuest,
    getGuest,
    deleteGuest,
} = require('./lib/api.js');
let createdGuestId;


describe('test replaceGuest PUT /guests/{wsuID}', function () {
    // current guest
    const guestData = {
        wsuID: '1234560',
        resident: true,
        zipCode: '01602',
        unemployment: false,
        assistance: {
            socSec: false,
            TANF: false,
            finAid: false,
            other: false,
            SNAP: false,
            WIC: false,
            breakfast: false,
            lunch: false,
            SFSP: false,
        },
        guestAge: 42,
        numberInHousehold: 4,
    }

    // Replacement guest for current guest
    const newGuestData = {
        wsuID: '1234560',
        resident: false,
        zipCode: '90210',
        unemployment: true,
        assistance: {
            socSec: true,
            TANF: false,
            finAid: true,
            other: false,
            SNAP: true,
            WIC: false,
            breakfast: true,
            lunch: true,
            SFSP: false,
        },
        guestAge: 30,
        numberInHousehold: 2,
    }

    before(async function () {
        // Create a guest before running the tests
        await createGuest(guestData)
        createdGuestId = guestData.wsuID
    })

    after(async function () {
        // Delete guest after running the tests
        await deleteGuest(createdGuestId)
    })

    it('should replace the guest with the given ID', async function () {
        const replaceRes = await replaceGuest(createdGuestId, newGuestData)
        replaceRes.should.have.status(200)
        replaceRes.body.should.deep.equal(newGuestData)

        // Check if the guest data is successfully replaced
        const getRes = await getGuest(createdGuestId)
        getRes.should.have.status(200)
        getRes.body.should.deep.equal(newGuestData)
    })

    it('should report that guest is not found', async function () {
        const invalidGuestId = '1234565'
        const res = await replaceGuest(invalidGuestId, newGuestData)
        res.should.have.status(404)
        res.body.should.have.property('error').equal('Guest not found')
        res.body.should.have.property('message').equal('wsuID does not exist')
    })

    it('should return an error if the guest ID is invalid', async function () {
        const invalidGuestId = 'abc123'
        const res = await replaceGuest(invalidGuestId, newGuestData)
        res.should.have.status(400)
        res.body.should.have
            .property('message')
            .equal('request.params.wsuID should match pattern "^[0-9]{7}$"')
        res.body.errors[0].should.have.own
            .property('errorCode')
            .equal('pattern.openapi.validation')
    })
})
