// Chai
//   - Provides a plugable fluent assertion library. We use its expect BDD style.
//   - Related identifiers: expect, use, to, be, have, equal, ...
//   - <https://www.chaijs.com/api/bdd/>
const chai = require('chai')
module.exports = chai
