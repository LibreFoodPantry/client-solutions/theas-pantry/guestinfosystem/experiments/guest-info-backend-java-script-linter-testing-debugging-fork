const chaiRequest = require('./chaiRequest.js')

module.exports = {
    // request for listing guest
    async listGuests(statusCode) {
        let endpoint
        if (statusCode === 200) {
            endpoint = '/guests'
        } else if (statusCode === 400) {
            endpoint = '/guests/bad-request'
        } else {
            throw new Error('Invalid status code')
        }
        try {
            const response = await chaiRequest.get(endpoint)
            return response
        } catch (error) {
            return error.response
        }
    },
    
    // request for getting API version
    async getAPIVersion() {
        try {
            const response = await chaiRequest.get('/version')
            return response
        } catch (error) {
            return error.response
        }
    },
    
    // request for deleting guest
    async deleteGuest(guestId) {
        try {
            const response = await chaiRequest.delete(`/guests/${guestId}`)
            return response
        } catch (error) {
            return error.response
        }
    },

    // request for replacing guest
    async replaceGuest(guestId, updatedGuestData) {
        try {
            const response = await chaiRequest
                .put(`/guests/${guestId}`)
                .send(updatedGuestData)
            return response
        } catch (error) {
            return error.response
        }
    },
    
    // request for creating guest
    async createGuest(guestData) {
        try {
            const response = await chaiRequest.post('/guests').send(guestData)
            return response
        } catch (error) {
            return error.response
        }
    },
    
   // request for retrieving guest
    async getGuest(id) {
        try {
            const response = await chaiRequest.get('/guests/' + id)
            return response
        } catch (error) {
            return error.response
        }
    },
}
