const { deleteGuest, createGuest } = require('./lib/api.js');
let createdGuestId;

describe('test deleteGuest DELETE /guests/{wsuID}', function () {
    const guestData = {
        wsuID: '1234564',
        resident: true,
        zipCode: '01602',
        unemployment: false,
        assistance: {
            socSec: false,
            TANF: false,
            finAid: false,
            other: false,
            SNAP: false,
            WIC: false,
            breakfast: false,
            lunch: false,
            SFSP: false,
        },
        guestAge: 42,
        numberInHousehold: 4,
    }

    before(async function () {
        await createGuest(guestData)
        createdGuestId = guestData.wsuID
    })

    it('should delete the guest with the given ID', async function () {
        const res = await deleteGuest(createdGuestId)
        res.should.have.status(200)
        res.body.should.deep.equal(guestData)
    })

    it('should report that guest is not found', async function () {
        const res = await deleteGuest(createdGuestId)
        res.body.should.have.property('status').equal(404)
        res.body.should.have.property('error').equal('Guest not found')
        res.body.should.have.property('message').equal('wsuID does not exist')
    })

    it('should return an error if the guest ID is invalid', async function () {
        const invalidGuestId = 'abc123'
        const res = await deleteGuest(invalidGuestId)
        res.should.have.status(400)
        res.body.should.have
            .property('message')
            .equal('request.params.wsuID should match pattern "^[0-9]{7}$"')
        res.body.errors[0].should.have.own
            .property('errorCode')
            .equal('pattern.openapi.validation')
    })
})
