#!/usr/bin/env bash

SCRIPT_DIR="$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"
cd "$SCRIPT_DIR/.." || exit

docker run --rm -v "${PWD}":/app/project -w /app/project \
    --entrypoint "" stoplight/spectral \
    spectral lint -F warn ./**/*openapi*.{yml,yaml}
